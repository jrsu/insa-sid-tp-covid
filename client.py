#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):#declaration du constructeur de la classe qui prend message catalogue comme parametre on initialise debug boolean a false et defaultProtocol a "http://")
		self.catalog = MessageCatalog(catalog_path)#initialise un catalogue de message 
		if debug:
			print(self.catalog)# si le debug est true on affiche le catalogue, si le bug est tjrs la on n'affiche rien
		self.r = None #declare r vide 
		self.debug = debug #fait le lien entre la variable d'instance debug et le parametre debug reçu par le constructeur, par default False
		self.protocol = defaultProtocol #fait le lien entre la variable d'instance protocol et le parametre defaultProtocol reçu par le constructeur, ici le protocole par defaut sera http
  #le decorateur @classmethod sert à utiliser cette méthode directement dans le constructeur de message (def__init__)
	@classmethod
	def withProtocol(cls, host):#methode qui definie la presence de protocole http
		res = host.find("://") > 1
        #cherche dans la chaine de caractère le morecau qui correspond au protocole 
        #cherche dans host la valeur du 1er  qui match le chemin "://" et qui est presente c'est a dire differente de -1 et donc >1
		return res# retourne res

	def completeUrl(self, host, route = ""):#methode qui insere la route dans l'url pour le completer
		if not Client.withProtocol(host):#cette condition if cherche à ajouter le protocole si jamais la methode precedente de recherche de protocole n'en trouve pas d'en le host
			host = self.protocol + host #ajoute dans host le protocol
		if route != "":#si la valeur de route inserée n'est pas vide lors de l'appel a la methode alors 
			route = "/"+route#definie le chemin de la route tel que: "/ + route"
		return host+route

	def say_something(self, host):#envoi un I said message au host(l'ajoute)
		m = Message()#declare m comme un message (classe Message)
		self.catalog.add_message(m)# ajoute m au catalogue de message
		route = self.completeUrl(host, m.content)#route prend la valeur retourner par la methode completeUrl qui prend m.content comme route
		self.r = requests.post(route)#r est l'envoi de la requete post a l'url route, celle ci permet d'envoyer de la data au serveur host
		if self.debug:#debug est initialisé à false, donc ici on verifie qu'il est bien true c'est a dire qu'il n'y a pas de bug
			print("POST  "+route + "→" + str(self.r.status_code))#affichage de l'instruction post avec la route envoyée et son status, on utilise POST pour créer une ressource
			print(self.r.text)#affichage de la requete
		return self.r.status_code == 201
    #statut retourné est de 201 quand la condition if est remplie, c'est a dire que la requête traitée avec succès et création du nouveau message
    #code 201: Created, indique qu'une ressource a été créé
    
	#add to catalog all the covid from host server
	def get_covid(self, host): # Fonction qui récupère les messages (type "ils ont dit") des personnes qui ont eu le covid 
		route = self.completeUrl(host,'/they-said') # Création de la route 
		self.r = requests.get(route) # Création de la requête
		res = self.r.status_code == 200 # Si la ressource est trouvée (statut de requête égale à 200), res prend la valeur True 
		if res: # Si la ressource est trouvée alors 
			res = self.catalog.c_import(self.r.json()) # Importation des messages dans catalogue 
		if self.debug: # Si il y a un bug on affiche la route 
			print("GET  "+ route + "→" + str(self.r.status_code)) # GET pour la recupération de donnée, de la route et statut de la requête 
			if res != False: # Si la requête est True alors
				print(str(self.r.json())) # Affichage de la requête 
		return res # Retourne le statut de la requête 

	#send to server list of I said messages
	def send_history(self, host): # Fonction qui envoie les messages (type "j'ai dit") au serveur 
		route = self.completeUrl(host,'/they-said') # Création de la route 
		self.catalog.purge(Message.MSG_ISAID) # Suppression des messsages anciens 
		data = self.catalog.c_export_type(Message.MSG_ISAID) # Exportation des messages (type "j'ai dit")
		self.r = requests.post(route, json=data) # Création de la requête
		if self.debug: # Si il y a un bug on affiche la route, la requête et les messages 
			print("POST  "+ route + "→" + str(self.r.status_code)) # POST pour poster les données( POST créé une ressource), la route et le statut de la requête 
			print(str(data)) # Affichage des données 
			print(str(self.r.text)) # Affichage de la requête 
		return self.r.status_code == 201 # Retourne True si la requête a été traité avec succès et on a créé un document


if __name__ == "__main__": # Méthode principale pour faire les testes 
	c = Client("client.json", True) # Création client debug 
	c.say_something("localhost:5000") # Création des messages dit par le client 
	c.get_covid("localhost:5000") # Récupération par le client des messages des personnes ayant eu le covid 
	c.send_history("localhost:5000") # Client envoie au serveur les messages type "il a dit"
