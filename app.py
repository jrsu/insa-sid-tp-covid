#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
#on utilise ici le décorateur route pour indiquer à flask le chemin de la ressource pris en charge( quel URL suivre)  
#on presise ici la méthodes GET puisqu'on veut ici récupérer un représentation de la ressource 
@app.route('/', methods=['GET'])
def index():
	response = render_template("menu.html",root=request.url_root, h="")
    #ici, render_template va nous servir à utiliser un fichier externe (en l'occurence "menu.html") pour stocker les reponses 
    #les reponse sont obtenue avec le module request de flask
    #ce module va en effet gérer les différentes requetes http
	return response

	#case 2 hear message
#comme précedement, on utilise ici le décorateur route pour indiquer à flask le chemin de la ressource pris en charge( quel URL suivre)  
#On utilise cette fois-ci le méthode POST qui va créer une ressource 
@app.route('/<msg>', methods=['POST'])
def add_heard(msg):
#ici, on commence par definir un première route pour gerer les requetes entre deux telephone I_hear et I_say 
#Pour s'assurer que le client a bien reçu le message qu'on lui à envoyer suite à sa requete, 
#on contrôle d'abord si dans le catalogue de message, on a bien le message qui a été créer

	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)):
        #si c'est le cas on repond client en envoyant un message attestant de la reception avec un texte et un statut de reponse
#ici on defini un statut 200 qui indique le succès de la requête 
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201)
	else :
#sinon, on envoie une reponse au client avec le statut 400 (Bad request) indique que le serveur ne peut pas comprendre la requête en raison d'une syntaxe invalide. Le client doit donc changer la requete si il veut la renvoyer.

		reponse = Response(status=400)
	return response
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST'])
#ensuite on va definir les deux routes entre le samrtphone et l'hopital 
#une pour declarer qu'un patient à le covid et informer l'hopital (methode POST)
#et une pour interroger la liste de l'hopital pour savoir si on est entré en contact avec un patient atteind du covid
#donc soit la methode de la requete est GET soit POST afin d'identifier les deux actions
def hospital():
    #on commence par la méthode GET
	if request.method == 'GET':
		#Wants the list of covid messages (they said)
        #on commence par enlever tout les messages du catalogue qui ont date de plus de 14 jours 
		client.catalog.purge(Message.MSG_THEY)
    #le client va ensuite obtenir l'information pour savoir si il est positif
    #on utilise encore une fois la methode Reponse de flasqeu qui va repondre à la requete du client
    #se dernier va envoyer donc le catalogue de message THEY_SAID sous forme de fichier json
    #toujours accompagner du statut 200 qui indique un succès de la requete 
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200)
	#on continue ici par l'autre methode POST, qui permet de declarer si une personne à le covid ou non
    elif request.method == 'POST':
    #si la requete est un fichier json 
		if request.is_json:
            #on recupère le catalogue existant des message THEY_SAID et stock ce fichier dans rep
			req = json.loads(request.get_json())
            #on va ensuite inserer le nouveau Message THEY_SAID dans le catalogue req
            #ce message est insere via la methode c_import_as_type de catalogue qui permet d'inserer le message dans le catalogue
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY)
            #on envoie ensuite la response à la requete au client en affichant que le message a bien été ajouter au catalogue
            #cette reponse va afficher un message de type texte ainsi qu'un statut 200, succès requete 
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)
		else:
            #si la requete ne trouve pas de ficheier json,
            #on renvoie une erreur 400: bad request 
			response = Response(f"JSON expected", mimetype='text/plain', status=400)
	#si la méthode de la requete n'est ni GET ni POST, 
    #on renvoie un reponse au client indiquant que la methode de la requet est interdite avec un statut
    #403: accés interdit 
    else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403)
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
