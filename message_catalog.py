#!/usr/bin/env python3
import json, os
from message import *

class MessageCatalog: #on crée la classe MessageCatalog
	#constructor, loads the messages (if there are any)
	#in this implementation we store all messages in a json file
	def __init__(self, filepath): #on initialise la classe
		#            I_SAID,I_HEARD,THEY
		self.data = [  []  ,  []   , [] ] #on crée la liste  de 3 listes contenant les messages ISAID, IHEARD et THEY 
		self.filepath = filepath #on attribue à notre self le chemin d'accès renseigné dans l'appel à la méthode
		if os.path.exists(self.filepath): #si fichier existe
			self.open() #on ouvre le fichier
		else:
			self.save()  #sinon on le met à jour 
            
	
	def __del__(self): #cette méthode supprime un fichier
		del self.filepath #on supprime le chemin d'accès du fichier 
		del self.data #on supprime ses données

	
	def get_size(self, msg_type=False): #cette méthode récupère la taille du catalogue(nombre de messages)
		if(msg_type is False):
			res = self.get_size(Message.MSG_ISAID) + self.get_size(Message.MSG_IHEARD) + self.get_size(Message.MSG_THEY) 
            #on récupère la somme des tailles des trois types de messages (méthode recursive)
		else:
            #si message.type non faux ( en réalité vaut 0,1,2)
			res = len(self.data[msg_type]) #sinon on récupère la longueur de la donnée dans la colonne msg_type
		return res #on retourne le résultat


	def c_import(self, content, save=True): #cette méthode importe le contenu du message
		i = 0 #initialisation
		for msg in content: #pour tous les messages dans la colonne content
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #si on a un message où save=false ie non enregistré dans le catalogue, on ajoute ce message
					i=i+1 #on implémente de 1 notre compteur
		if save: #si save=true
			self.save() #on met à jour ce message en l'enregistrant à nouveau
		return i #on retourne la valeur notre compteur

	
	def c_import_as_type(self, content, new_type=Message.MSG_THEY): #cette méthode importe le contenu d'un message extérieur tout en lui affiliant le type THEY (en gros on récupère les messages transmis par les autres et on les désigne comme tels dans leur type)
		i=0 #initialisation
		for msg in content: #pour tous les messages dans la colonne content
			m = Message(msg) #on stock le message dans m
			m.set_type(new_type) #on affilie un nouveau type à m
			if self.add_message(m, False): #si le nouveau type de m n'est pas mis à jour ie save=false
				i=i+1 #on implémente notre compteur de 1
		if i > 0: #si notre compteur est sup à 0 ie on a déjà importé les messages sous ce type là
			self.save() #on met à jour ce message
		return i #on retourne la valeur de notre compteur


	def open(self): #cette méthode ouvre le message depuis le fichier
		file = open(self.filepath,"r") #on stock le message ouvert en mode lecture dans le fichier dans file
		self.c_import(json.load(file), False) #on importe le contenu du message stocké dans la variable file au format json
		file.close() #on ferme le fichier

	
	def c_export_type(self, msg_type, indent=2, bracket=True): #on exporte les message d'un certain type depuis notre catalogue
		tmp = int(bracket) * "[\n"
		first_item = True #on initialise la valeur de first_item à true
		for msg in self.data[msg_type]: #pour une donnée dans la colonne type de message
			if first_item: #si la valeur first_item est true
				first_item=False #on passe la variable first_itm de true à false
			else:  #si sa valeur est déjà false
				tmp = tmp +",\n"  #on va à la ligne
			tmp = tmp + (indent*" ") +  msg.m_export() #on incrémente tmp de la valeur du message exporté en le séparant d'un espace
		tmp = tmp + int(bracket) * "\n]" #on va à la ligne
		return tmp 


	def c_export(self, indent=2): #on exporte le catalogue sous un format texte
		tmp = ""
		if(len(self.data[Message.MSG_ISAID])> 0 ): #si la longueur du message ISAID est non nulle
			tmp = tmp + self.c_export_type(Message.MSG_ISAID, indent, False) #on exporte ce message de type ISAID
		if(len(self.data[Message.MSG_IHEARD]) > 0): #si la longueur du message IHEARD est non nulle
			if tmp != "": 
				tmp = tmp + ",\n" #on va à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_IHEARD, indent, False) #on exporte ce message de type IHEARD 
		if(len(self.data[Message.MSG_THEY]) > 0): #si la longueur du message THEY est non nulle
			if tmp != "":
				tmp = tmp + ",\n" #on va à la ligne
			tmp = tmp + self.c_export_type(Message.MSG_THEY, indent, False) #on exporte ce message de type THEY
		tmp = "[" + tmp + "\n]" #on retourne à la ligne
		return tmp

	#a method to convert the object to string data
    #renvoie un paragraphe contenant le catalogue de messsage 
    #ce tmp est structuré comme un document xml avec ouverture et fermeture de balise, tabulation 
    #il contient les message Isaid, Iheard et They 
	def __str__(self):
		tmp="<catalog>\n"
		tmp=tmp+"\t<isaid>\n"
		for msg in self.data[Message.MSG_ISAID]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</isaid>\n\t<iheard>\n"
		for msg in self.data[Message.MSG_IHEARD]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</iheard>\n\t<theysaid>\n"
		for msg in self.data[Message.MSG_THEY]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</theysaid>\n</catalog>"
		return tmp


	def save(self): #cette méthode sert à sauvegarder le contenu de l'objet dans le fichier
		file = open(self.filepath,"w") #on ouvre le fichier en mode écriture 
		file.write(self.c_export()) #on applique au fichier la méthode c_export pour exporter le catalogue sous format texte
		file.close() #on ferme le fichier
		return True


	def add_message(self, m, save=True): #cette méthode à jour un objet message au catalogue
		res = True
		if(self.check_msg(m.content, m.type)): #si le message est deja dans la liste  
			print(f"{m.content} is already there") #on affche le contenu du message en indiquant qu'il est deja present 
			res = False
		else:
			self.data[m.type].append(m) #sinon on ajoute le message dans la liste de son type 
			if save: #si le message est déjà sauvé
				res = self.save() #on le sauvegarde à nouveau dans notre variable res
		return res #onon retroune notre variable res


	def purge(self, max_age=14, msg_type=False): #cette méthode supprime tous les message de msg_type qui sont périmés (ie ici dont l'âge est supérieur à 14j)
		if(msg_type is False): #si le msg_type est faux
			self.purge(max_age, Message.MSG_ISAID) #on supprime le message du type ISAID (par recursivité)
			self.purge(max_age, Message.MSG_IHEARD) #on supprime le message du type IHEARD (par recursivité)
			self.purge(max_age, Message.MSG_THEY) #on supprime le message du type THEY (par recursivité)
		else: #si le msg_type n'est pas faux (soit 0,1,2)
			removable = [] #on initialise un tableau contenant les message enlevés 
			for i in range(len(self.data[msg_type])): #on parcourt tous les messages
				if(self.data[msg_type][i].age(True)>max_age): #si l'âge du message est supérieur à la date de péremption
					removable.append(i) #on ajoute le message au tableau initialisé
			while len(removable) > 0: #tant que la longueur de notre tableau des message périmé n'est pas vide
					del self.data[msg_type][removable.pop()] #on le supprime de data 
			self.save() #on sauvegarde la manip
		return True


	def check_msg(self, msg_str, msg_type=Message.MSG_THEY): #cette méthode vérifie si un certain message appartient deja a la liste de message
		for msg in self.data[msg_type]: #on parcourt tous les messages du type (par defaut THEY)
			if msg_str == msg.content: #si le texte du message THEY est le même que le contenu du message qu'on voulait vérifier
				return True #on retourne vrai
		return False#sinon faux 

	
	def quarantine(self, max_heard=4): #cette méthode nous dit si l'on doit se mettre en quarantaine au vu de l'état du catalogue
		self.purge() #on supprime tous les messsages périmés
		n = 0 #initialisation 
		for msg in self.data[Message.MSG_IHEARD]: #pour tous les messages entedus (de type IHEARD)
			if self.check_msg(msg.content): #si le contenu entendu est le même que celui dans THEY
				n = n + 1 #on incrémente
		print(f"{n} covid messages heard") #on prévient l'utilisateur qu'il a entendu une personne covidée
		return max_heard < n #on retourne le nombre de personnes entendues et si ce dernier est supérieur à 4

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_IHEARD))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_THEY}, \"date\":{time.time()}}}"))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog)
