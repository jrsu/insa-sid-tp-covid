#!/usr/bin/env python3
#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#some constants (message types)
	MSG_ISAID = 0 	#What-I-said message
	MSG_IHEARD = 1	#What-I-heard message
	MSG_THEY = 2	#What-covid-19-infected-said message
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	#otherwise, if date is False the time is system time
		#else it is a float, then the date should be this parameter
        
        
	def __init__(self, msg="", msg_type=False, msg_date=False):
		if msg == "": #si le message est vide ce qui est théoriquement le cas
			self.content = Message.generate() #on génère un message (création) 
			self.type = Message.MSG_ISAID #on attribue à ce message le type MSG_ISAID soit la valeur 0 (attribution d'un critère de valeur)
			self.date = time.time() #on attribue à ce message la date et l'heure à laquelle il vient d'être généré (attribution d'un critère de date)
		elif msg_type is False : #si le msg_type est faux ce qui est théoriquement le cas
			self.m_import(msg) #on importe le contenu du message (recupère ces valeurs au format json)
		else: #si le message est non vide et que msg_type différent de faux,le message existe deja 
			self.content = msg () #on attribue au message le même contenu que celui du message qui est non vide
			self.type = msg_type #on lui attribue aussi ton type
			if msg_date is False: #mais si ce message n'a pas encore d'attribut de date
				self.date = time.time() #on lui en crée un
			else:
				self.date = msg_date #sinon on lui affilie la date qui est déjà attribuée au message
                
            

	#a method to print the date in a human readable form
	def hdate(self):
		return time.ctime(self.date) #nous donnne une date en temps réel
    

	#a method to compute the age of a message in days or in seconds
	#for display purpose, set as_string to True
	def age(self, days=True, as_string=False):
		age = time.time() - self.date #on calcule l'âge en faisant la différence entre le temps actuel au moment de l'appel de la méthode et la date du message
		if days: #si days=True ce qui est théoriquement le cas
			age = int(age/(3600*24)) #on attribut à age une valeur en jours
		if as_string:
			d = int(age/(3600*24))  #on en calcul le nombre de jours entier 
			r = age%(3600*24)#on retient le reste de la division ( modulo jour )
			h = int(r/3600) #on en calcul le nombre d'heure entière
			r = r%3600 #on restient egalement le reste du temps a convertir (modulo heure)
			m = int(r/60) #on calcul le nombre de minutes
			s = r%60 #est le reste de la division correpsond au nombre de seconde 
			age = (str(age)+"~"+str(d)+"d"+ str(h) + "h"+str(m)+"m"+str(s)+"s")#on donne à âge un format indiquant le nombre de jours, d'heures, de minutes et de secondes
		return age

	#testers of message type
    #return le type du constructeur self.type
	def is_i_said(self):
		return self.type == Message.MSG_ISAID 
	def is_i_heard(self):
		return self.type == Message.MSG_IHEARD
	def is_they_said(self):
		return self.type == Message.MSG_THEY

	#setters
	def set_type(self, new_type):
		self.type = new_type #affecte un type à un message

	#a class method that generates a random message
    #le decorateur @classmethod sert à utiliser cette méthode directement dans le constructeur de message (def__init__)
	@classmethod
	def generate(cls):
		return sha256(bytes(str(time.time())+str(random()),'utf8')).hexdigest() 
    #on génère un message aléaoire tout d'abord en lui affectant le moment ou il a été créer avec time.time()
    #ensuite, on lui rajoute une chaine de caractère aléatoire avec la fonction random (on aurait pu utiliser le module secret de pyhton)
    #enfin, on veint chiffrer le message selon le formalisme sha256 afin de proteger les données (en presisant le codage informatique des données utf-8)

	#a method to convert the object to string data
	def __str__(self):
        #retourne sous forme de texte le type, le contenu et la date du message 
		return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"

	#export/import
	def m_export(self):
		return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}" #on exporte un message et plus précisemment, son contenu, son type et sa date

	def m_import(self, msg): #on importe un message
		if(type(msg) == type(str())): #si le type du message est un string ie chaîne de caractères
			json_object = json.loads(msg) #on charge le message au format json
		elif(type(msg) == type(dict())): #si le type du message est un dictionnaire ie liste de clés associées à des valeurs
			json_object = msg #on prend directement la valeur associée à la clé du message pour la mettre dans json_object
		else:   
			raise ValueError #sinon on renvoie un message d'erreur 
		self.content = json_object["content"] #le contenu du message est placé dans la colonne content du fichier au format json
		self.type = json_object["type"] #le type du message est placé dans la colonne type du fichier au format json
		self.date = json_object["date"] #la date du message est placé dans la colonne date du fichier au format json

#will only execute if this file is run
if __name__ == "__main__":
	#test the class
	myMessage = Message() #on appelle la méthode message dans la variable myMessage
	time.sleep(1) #on reporte l'execution de 1sec
	mySecondMessage = Message(Message.generate(), Message.MSG_THEY) #on  appelle les messages générés par soi et les messages émis par ceux ayant été diagnostiqués positifs
	copyOfM = Message(myMessage.m_export()) #on estock les messages exportés dans copyOfM
	print(myMessage) #on affiche les messages
	print(mySecondMessage) #on affiche les messages générés et ceux transmis par les individus positifs au covid
	print(copyOfM) #on affiche les messages exportés depuis notre téléphone
	time.sleep(0.5) #on reporte l'execution de 0.5 sec
	print(copyOfM.age(True,True)) #on affiche l'âge des messages exportés depuis notre téléphone en indiquant le nombre de jours, d'heures, de minutes et de secondes
	time.sleep(0.5) #on reporte l'execution de 0.5 sec
	print(copyOfM.age(False,True)) #on affiche l'âge des messages exportés sans rentrer dans la boucle if days.
