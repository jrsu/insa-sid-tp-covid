#!/usr/bin/env python3
def falsifytime(): # Fonction pour donner l'heure dans le monde de notre projet (1min=14jours dans le monde du projet)
	import time as time
	#the referencetime is when we load this module
	referencetime = time.time() # referencetime est notre heure de référence c'est à dire t=0 et la fonction time() donne une valeur en seconde quand on charge un message 
	#factor is how many seconds of fake time correspond to a real second
	factor = 3600*24*14/60 #one real minute is fourteen days of fake time

	#override time.time()
	real_time = time.time # Récupération de l'heure actuelle (heure réelle)
	def fake_time(): # Fonction qui donne l'heure dans le monde du projet (l'heure fictive) à partir de l'heure réelle 
		duration = real_time() - referencetime # Donne le temps écoulé entre le chargement du programme (t=0) et l'heure actuelle 
		return referencetime + factor*duration # Retourne l'heure actuelle dans le monde du projet (ce n'est pas l'heure réelle)
	time.time = fake_time # Définit l'heure actuelle réelle comme l'heure actuelle dans le monde du projet 
	return time # Retourne l'heure du message modifiée 
